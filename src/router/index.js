import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Browse from "@/views/Browse";
// import Player from '@/views/Player'
import Help from "@/views/Help";
import Login from "@/views/Login";
import Signup from "@/views/Signup";
import ResetPassword from "@/views/ResetPassword";
import Account from "@/views/Account";
// import Course from '@/views/Course'
import MyCourses from "@/views/MyCourses";

// import Steppers from '@/views/Steppers'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/browse",
    name: "browse",
    component: Browse
    // component: () => import(/* webpackChunkName: "browse" */ '../views/Browse')
  },
  {
    path: "/help",
    name: "help",
    component: Help
    // component: () => import(/* webpackChunkName: "library" */ '../views/Help')
  },
  {
    path: "/course/:id",
    name: "course",
    // component: Course,
    component: () => import(/* webpackChunkName: "player" */ "../views/Course")
  },
  {
    path: "/player/:id",
    name: "player",
    // component: Player,
    component: () => import(/* webpackChunkName: "player" */ "../views/Player"),
    meta: { requireAuth: true }
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: { isLoggedOut: true }
  },
  {
    path: "/signup",
    name: "signup",
    component: Signup,
    meta: { isLoggedOut: true }
  },
  {
    path: "/reset-password",
    name: "resetPassword",
    component: ResetPassword,
    meta: { isLoggedOut: true }
  },
  {
    path: "/account",
    name: "account",
    component: Account,
    meta: { requireAuth: true }
  },
  {
    path: "/my-courses",
    name: "my-courses",
    component: MyCourses,
    meta: { requireAuth: true }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireAuth)) {
    // Need to login
    if (
      !(
        localStorage.getItem("jwt") ||
        localStorage.getItem("userEmail") ||
        localStorage.getItem("userName")
      )
    ) {
      next({
        name: "login"
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.isLoggedOut)) {
    //  Need to Logout
    if (
      localStorage.getItem("jwt") &&
      localStorage.getItem("userEmail") &&
      localStorage.getItem("userName")
    ) {
      next({
        name: "home"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
