import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    jwt: localStorage.getItem("jwt"),
    user: {
      name: localStorage.getItem("userName"),
      email: localStorage.getItem("userEmail")
    }
  },
  mutations: {
    login(state, user) {
      state.loggedIn = true;
      state.user.name = user.name;
      state.user.email = user.email;
      state.user.stripe_user_id = user.stripe_user_id;
    }
  },
  actions: {
    login(context) {
      const user = {};
      context.commit("login", user);
    },
    logout() {
      this.state.loggedIn = false;
      this.state.user.name = undefined;
      this.state.user.email = undefined;
      localStorage.clear();
    }
  },
  modules: {},
  getters: {
    isLoggedIn: state => {
      return !!(state.jwt && state.user.email && state.user.name);
    }
  }
});
