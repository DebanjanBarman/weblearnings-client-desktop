module.exports = {
  HOME_VIEW: "https://sekho.herokuapp.com/api/v1/courses",
  // HOME_VIEW: 'http://localhost:3001/api/v1/courses',
  //
  SIGN_UP: "https://sekho.herokuapp.com/api/v1/users/signup",
  // SIGN_UP: 'http://localhost:3001/api/v1/users/signup',
  //
  LOG_IN: "https://sekho.herokuapp.com/api/v1/users/login",
  // LOG_IN: 'http://localhost:3001/api/v1/users/login',
  //
  COURSE: "https://sekho.herokuapp.com/api/v1/courses",
  // COURSE: 'http://localhost:3001/api/v1/courses',
  //
  COURSE_PREVIEW: "https://sekho.herokuapp.com/api/v1/courses/preview",
  // COURSE: 'http://localhost:3001/api/v1/courses/preview',
  //
  UPDATE_PROFILE: "https://sekho.herokuapp.com/api/v1/users/updateMe",
  // UPDATE_PROFILE: 'http://localhost:3001/api/v1/users/updateMe',
  //
  UPDATE_PASSWORD: "https://sekho.herokuapp.com/api/v1/users/updateMyPassword",
  // UPDATE_PASSWORD: 'http://localhost:3001/api/v1/users/updateMyPassword',
  //
  DELETE_ME: "https://sekho.herokuapp.com/api/v1/users/deleteMe",
  // DELETE_ME: 'http://localhost:3001/api/v1/users/deleteMe'
  //
  GET_ME: "https://sekho.herokuapp.com/api/v1/users/me",
  // GET_ME: 'http://localhost:3001/api/v1/users/me'
  //
  GET_MY_COURSES: "https://sekho.herokuapp.com/api/v1/users/my-courses",
  // GET_MY_COURSES: 'http://localhost:3001/api/v1/users/my-courses'
  //
  FORGOT_PASSWORD: "https://sekho.herokuapp.com/api/v1/users/forgotPassword",
  // FORGOT_PASSWORD: 'http://localhost:3001/api/v1/users/forgotPassword',
  //
  RESET_PASSWORD: "https://sekho.herokuapp.com/api/v1/users/resetPassword",
  // RESET_PASSWORD: 'http://localhost:3001/api/v1/users/resetPassword',
  //
  // PURCHASE_COURSE: 'http://localhost:3001/api/v1/purchases/checkout-session',
  PURCHASE_COURSE:
    "https://sekho.herokuapp.com/api/v1/purchases/checkout-session",
  //
  // ENROLL_IN_FREE_COURSE: 'localhost:3001/api/v1/purchases/free-course',
  ENROLL_IN_FREE_COURSE:
    "https://sekho.herokuapp.com/api/v1/purchases/free-course",

  STRIPE_PUBLIC_KEY:
    "pk_live_51HddEjGdXQnnBe728n77iSMacZ18IWoMLO61SwLHgiAqdiwAAaaQVFq68sBIPYByPumogS16qSOioesAXIW0851b00YOGsYjcT"
};
